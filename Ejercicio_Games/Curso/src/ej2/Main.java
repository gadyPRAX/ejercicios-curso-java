package ej2;

import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String args[]) {
		List<Alumno> listA = new ArrayList<>();
		char letraA = 97;
		for (int i = 0; i < 20; i++) {
			if(i%2 == 0) {
				listA.add(new Alumno("nombre"+letraA,"ap"+i,"am"+i,"curso"+i));
			}else {
				listA.add(new Alumno("nombre"+letraA+i,"ap"+i,"am"+i,"curso"+i));
			}
		}

		listA.forEach(a -> System.out.println(a));
		System.out.println("\n" + listA.size() + "\n");
//		for(Alumno a:listA) {
//			if(a.charAt(a.nombre.lastIndexOf("2")).equals("2")) {
//				
//			}
//		}
		listA.stream().filter(a -> a.nombre.endsWith("a")).forEach(System.out::println);
		System.out.println("");
		listA.stream().limit(5).forEach(System.out::println);
	}
}
