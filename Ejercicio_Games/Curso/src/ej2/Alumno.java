package ej2;

public class Alumno {	
	public Alumno (String nombre, String apellidoPaterno, String apellidoMaterno, String nombreCurso){
		this.nombre = nombre;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.nombreCurso = nombreCurso;
	}
	
	public Alumno (){

	}
	
	String nombre;
	String apellidoPaterno;
	String apellidoMaterno;
	String nombreCurso;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getNombreCurso() {
		return nombreCurso;
	}
	public void setNombreCurso(String nombreCurso) {
		this.nombreCurso = nombreCurso;
	}
	
	@Override
	public String toString() {
		return  "Nom: " + this.nombre +
				"\tApellido Paterno: " + this.apellidoPaterno +
				"\tApellido Materno: " + this.apellidoMaterno +
				"\tCurso: " + this.nombreCurso;
	}
}
