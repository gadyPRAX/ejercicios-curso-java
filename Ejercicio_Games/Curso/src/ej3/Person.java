package ej3;

public class Person {
	String nombre;
	Long id;
	int algo;
	
	public int getAlgo() {
		return algo;
	}
	public void setAlgo(int algo) {
		this.algo = algo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return "Nombre: " + this.nombre +
				"\nID: " + this.id +
				"\nAlgo: " + this.algo;
	}
}
