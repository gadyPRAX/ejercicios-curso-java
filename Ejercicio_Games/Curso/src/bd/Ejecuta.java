package bd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Ejecuta {
	static DAO dao;
	public static void main (String arg[]) {
		Persona una ;
		try (Connection conn = DriverManager.getConnection(
                "jdbc:oracle:thin:@GADY-PC:1521:RAP", "SPID", "SPID")) {
			dao = DAO.newInstance(false);
            if (conn != null) {
                System.out.println("Connected to the database!");
            } else {
                System.out.println("Failed to make connection!");
            }
            
            dao.obtenTodos().stream().forEach(System.out::println);
            
            List<Persona> personas = new ArrayList<>();
            
            una = new Persona("prueba", "prueba", "prueba");
            personas.add(una);
            una = new Persona("prueba2", "prueba2", "prueba2");
            personas.add(una);
            System.out.println(dao.insertAll(personas));
            
            personas.clear();
            una = new Persona("sergio", "actualiza", "actualiza");
            personas.add(una);
            System.out.println(dao.updateAll(personas));
            
            dao.deletePerson("sergio");
            
            
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }		
	}
}
