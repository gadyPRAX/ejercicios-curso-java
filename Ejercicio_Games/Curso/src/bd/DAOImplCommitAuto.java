package bd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DAOImplCommitAuto implements DAO {
	private final String obtenTodos = "select * from personas";
	private final String insertaPersona = "insert into personas (NOMBRE, APELLIDO_PARTERNO, APELLIDO_MATERNO) values (?, ?, ?)";
	private final String actualizaPersona = "update PERSONAS set NOMBRE=?, APELLIDO_PARTERNO=?, APELLIDO_MATERNO=? where NOMBRE=?";
	private final String borraPersona ="delete from PERSONAS where NOMBRE=?";

	public Connection getConmnection() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection("jdbc:oracle:thin:@GADY-PC:1521:RAP", "SPID", "SPID");
			if (conn != null) {
				System.out.println("Connected to the database!");
			} else {
				System.out.println("Failed to make connection!");
			}

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

	public List<Persona> obtenTodos() {
		List<Persona> lista = null;
		Persona persona;
		try (Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@GADY-PC:1521:RAP", "SPID", "SPID");) {
			try (PreparedStatement ps = conn.prepareStatement(obtenTodos); ResultSet rs = ps.executeQuery()) {
				lista = new ArrayList<>();

				while (rs.next()) {
					persona = new Persona(rs.getString("NOMBRE"), rs.getString("APELLIDO_PARTERNO"), rs.getString("APELLIDO_MATERNO"));
					lista.add(persona);
				}
			} catch (SQLException e) {
				conn.rollback();
				e.printStackTrace();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return lista;
	}

	public int insertAll(List<Persona> personas) {
		int insertados = 0;
		int registro = 0;
		
		try (Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@GADY-PC:1521:RAP", "SPID", "SPID");
				PreparedStatement ps = conn.prepareStatement(insertaPersona)) {
			for(Persona per: personas) {
				ps.setString(1, per.getNombre());
				ps.setString(2, per.getApellidoPaterno());
				ps.setString(3, per.getApellidoMaterno());
				registro = ps.executeUpdate();
				
				if(registro > 0) {
					insertados += registro;
					registro = 0;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return insertados;
	}

	public int updateAll(List<Persona> personas) {
		int actualizados = 0;
		int registro = 0;
		
		try (Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@GADY-PC:1521:RAP", "SPID", "SPID");
				PreparedStatement ps = conn.prepareStatement(actualizaPersona)) {
			for(Persona per: personas) {
				ps.setString(1, per.getNombre());
				ps.setString(2, per.getApellidoPaterno());
				ps.setString(3, per.getApellidoMaterno());
				ps.setString(4, per.getNombre());
				ps.addBatch();
				
				if(registro > 0) {
					actualizados += registro;
					registro = 0;
				}
			}
			ps.executeBatch();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return actualizados;
	}

	public boolean deletePerson(String nombre) {
		boolean res = false;
		int registro = 0;

		try (Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@GADY-PC:1521:RAP", "SPID", "SPID");
				PreparedStatement ps = conn.prepareStatement(borraPersona)) {
				ps.setString(1, nombre);
				registro = ps.executeUpdate();
				if(registro > 0) {
					res = true;
				}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return res;
	}
}
