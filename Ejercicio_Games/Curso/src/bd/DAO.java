package bd;

import java.sql.Connection;
import java.util.List;

public interface DAO {
	public Connection getConmnection();
	public List<Persona> obtenTodos();
	public int insertAll(List<Persona> personas);
	public int updateAll(List<Persona> personas);
	public boolean deletePerson(String nombre);
	
	static DAO newInstance(boolean auto) {
		if(auto) {
			return new DAOImplCommitAuto();
		}else {
			return new DAOImpl();
		}
	}
}
