package ej1;

public interface Gameable {
	void startGame();
	default void setGameName(String name) {
		System.out.println(name);
	}
}
