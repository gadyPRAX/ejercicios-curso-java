package ej1;

import java.util.Optional;

public interface Playeable {
	void walk(double x, double y);
	default void setGameName(String name) {
		System.out.println(Optional.of(name));
	}
}