package ej1;

import java.util.Optional;

public class PrincipalGame {
	static String gameName;
	static double playerPosx;
	static double playerPosY;
	
	public static void main(String args[]) {
		gameName =  "lol";
		Playeable p = (a1, a2) -> {
			playerPosx+= a1;
			playerPosY+= a2;
			System.out.println(playerPosx + " " + playerPosY);
		};
		
		Gameable g = () -> {
			p.setGameName(gameName);
		};
		
		Soundable s = (cancion) -> {
			System.out.println(Optional.ofNullable(cancion));
		};
		
		p.walk(1, 1);
		g.startGame();
		s.playMusic(null);
	}
}
