package pack;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Comparacion {
	public static void main(String args[]) {
		lambda();
		streams();
		tiempo();
		opcional();
	}
	
	static void lambda() {
		int ancho = 10;
		
		Pintable p = new Pintable(){
			public void pinta(){
			System.out.println("Pintando " + ancho);
			}
			};
			p.pinta();
			
			Pintable p1 = () -> {
				System.out.println("Pintando " + ancho);
				};
				p1.pinta();
	}
	
	static void streams() {
		List<Integer> numbers = Arrays.asList(1, 2, 3, 4);
		List<Integer> cuadrado = numbers
		.stream()
		.map(x -> x + x)
		.collect(Collectors.toList());
		System.out.println(cuadrado.toString());
	}
	
	static void tiempo() {
		LocalDate localDate = LocalDate.now();
		LocalTime localTime = LocalTime.of(12, 20);
		LocalDateTime localDateTime = LocalDateTime.now();
		
		System.out.println("localDate " + localDate + " localTime " + localTime + " localDateTime " + localDateTime);
		
		Instant instant = Instant.now();
		Instant instant1 = instant.plus(Duration.ofMillis(5000));
		Instant instant2 = instant.minus(Duration.ofMillis(5000));
		Instant instant3 = instant.minusSeconds(10);
		
		System.out.println("instant  " + instant);
		System.out.println("instant1 " + instant1);
		System.out.println("instant2 " + instant2);
		System.out.println("instant3 " + instant3);
	}
	
	static void opcional() {
		Optional<String> color = Optional.of("BLANCO");
		String colorValue1 = "negro";
		String colorValue2 = null;
		System.out.println("\n Optional no vacío " + color);
		System.out.println("Optional con el valor no nulo: Valor del color : "
		+ color.get());
		System.out.println("Optional vacio: " + Optional.empty());
		System.out.println("ofNullable en Optional no vacío: "
		+ Optional.ofNullable(colorValue1));
		System.out.println("ofNullable en un Optional vacío: "
		+ Optional.ofNullable(colorValue2));
		// java.lang.NullPointerException
		System.out.println("of en un Optional no vacío: " + Optional.of(colorValue2));
	}
}

interface Pintable{
void pinta();
}
